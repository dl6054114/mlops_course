## Code Formatting
Install [ruff](https://marketplace.visualstudio.com/items?itemName=charliermarsh.ruff).

Use `ruff format file` to format file.
