import numpy as np
import cv2


def read_image(imagepath: str) -> np.ndarray:
    """
    Read image in RGB format.
    Args:
        imagepath (str): Path to image.

    Returns:
        np.ndarray: RGB image.
    """
    return cv2.cvtColor(cv2.imread(imagepath))


def change_brightness(image1: np.ndarray, brightness_coef: int) -> np.ndarray:
    """
    Change brightness of image.

    Args:
        image1 (np.ndarray): Image to change brightness.
        brightness_coef (int): Amplitude of changing brightenss

    Returns:
        np.ndarray: Image with changed brightness.
    """

    return image1 + brightness_coef
